﻿using System;
using System.Collections.Generic;

namespace ScraperNET.Models
{
    [Serializable]
    public class AnimeModel
    {
        public string title;
        public string description;
        public int releaseDate;

        public string firstEpisode;
        public string imgLink;

        // one anime can have more generes
        public List<string> generes = new List<string>();
        // Episodes links
        public List<EpisodeModel> episodes = new List<EpisodeModel>();

        public bool scraped = false;
    }
}
