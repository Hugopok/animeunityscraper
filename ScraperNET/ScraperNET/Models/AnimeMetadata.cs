﻿using System.Collections.Generic;

namespace ScraperNET.Models
{
    public class AnimeMetadata
    {
        public int version = 0;

        public bool archiveCompletelyScraped;
        public bool rescrape;

        public List<AnimeModel> animes = new List<AnimeModel>(); 
    }
}
