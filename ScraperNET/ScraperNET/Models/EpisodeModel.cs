﻿using System.Collections.Generic;

namespace ScraperNET.Models
{
    public class EpisodeModel
    {
        public string episodeLink;
        public string urlVideo;
        public string title;

        public bool epScraped;
    }
}
