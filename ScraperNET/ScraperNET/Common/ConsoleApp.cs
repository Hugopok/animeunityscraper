﻿using System;
using Newtonsoft.Json;
using ScraperNET.Models;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ScraperNET.Common
{
    public class ConsoleApp
    {
        public static string JsonPath;

        public static AnimeMetadata metaData = new AnimeMetadata();

        public static void Main(string[] args)
        {
            Scrapers.AnimeUnityScraper animeUnity = new Scrapers.AnimeUnityScraper();
            //Scrapers.AnimeSaturnScraper animeSaturn = new Scrapers.AnimeSaturnScraper();

            Console.WriteLine("Args : " + args.Length);

            JsonPath = AppDomain.CurrentDomain.BaseDirectory + "/AnimeUnity.json";

            try
            {
                //animeSaturn.Start();
                animeUnity.Start(); 
                //animeUnity.Init();
            }
            catch (Exception e)
            {
                Console.WriteLine("Errore:    /n   " + e.Message + "/n" + args[0]);
            }
        }


        public static void RunNewInstance(AnimeMetadata metadata)
        {
            metadata.version++;

            var json = JsonConvert.SerializeObject(metadata, Formatting.None);

            File.WriteAllText(JsonPath, json);
           
            var exePath = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.FriendlyName);

            Console.WriteLine("Exe : " + exePath);

            metadata.version++;

            Process.Start(exePath);

            Environment.Exit(0);
        }
    }
}
 