﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScraperNET.Common
{
    public static class AnimeUnityRules
    {
        // Selector
        public const string ANIME_CSS_DIV_ARCHIVECONTAINER_SELECTOR = "div.archive-container";
        public const string ANIME_CSS_DIV_EPISODECONTAINER_SELECTOR = "div.ep-box";
        // Fields
        public const string ANIME_CSS_CLASS = "col-lg-4 col-md-6 col-sm-12";
        public const string ANIME_CSS_IMG_CLASS = "col-md-5 col-sm-5 archive-col";
        public const string ANIME_CSS_CONTENT_CLASS = "col-md-7 col-sm-7 archive-col";
        public const string ANIME_CSS_TITLE = "card-title";
        public const string ANIME_CSS_DESCRIPTION = "card-text archive-plot";
        public const string LINK = "href";
        public const string SRC = "src";
    }
}
