﻿using IronWebScraper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScraperNET.Utils
{
    public static class NodeHelper
    {
        public static List<HtmlNode> GetNodesFiltered(string filter, List<HtmlNode> nodesToFilter, bool searchForValue = true)
        {
            var nodes = new List<HtmlNode>();

            foreach (var child in nodesToFilter)
            {
                if (child == null) continue;

                foreach (var childNode in child.ChildNodes)
                {
                    if (childNode == null) continue;

                    HtmlNode childFiltered;
                    if (searchForValue)
                        childFiltered = GetNodeForValue(childNode, filter);
                    else
                        childFiltered = GetNodeForKey(childNode, filter);

                    if (childFiltered != null)
                        nodes.Add(childFiltered);
                }
                HtmlNode cF;
                if (searchForValue)
                    cF = GetNodeForValue(child, filter);
                else
                    cF = GetNodeForKey(child, filter);
                if (cF != null)
                    nodes.Add(cF);
            }

            return nodes;
        }

        public static HtmlNode GetNodeForValue(HtmlNode node, string stopTo)
        {

            if (node.Attributes.ContainsValue(stopTo))
            {
                return node;
            }

            foreach (var childNode in node.ChildNodes)
            {
                if (childNode == null) continue;
                if (childNode.Attributes.Count == 0) continue;

                if (childNode.Attributes.ContainsValue(stopTo))
                    return childNode;


                var nodeSearched = GetNodeForValue(childNode, stopTo);

                if (nodeSearched == null) continue;

                if (nodeSearched.Attributes.ContainsValue(stopTo))
                    return nodeSearched;
            }
            return null;

        }

        public static HtmlNode GetNodeForKey(HtmlNode node, string stopTo)
        {

            if (node.Attributes.ContainsKey(stopTo))
            {
                return node;
            }

            foreach (var childNode in node.ChildNodes)
            {
                if (childNode == null) continue;
                if (childNode.Attributes.Count == 0) continue;

                if (childNode.Attributes.ContainsKey(stopTo))
                    return childNode;


                var nodeSearched = GetNodeForKey(childNode, stopTo);

                if (nodeSearched == null) continue;

                if (nodeSearched.Attributes.ContainsKey(stopTo))
                    return nodeSearched;
            }
            return null;

        }

        public static string GetStringFromText(HtmlNode node, string key, string value)
        {
            if (node == null)
                return string.Empty;

            if (HasAttributeValue(node, key, value))
            {
                return node.TextContentClean;
            }

            foreach (var child in node.ChildNodes)
            {
                if (child == null)
                    continue;

                if (child.Attributes.Count <= 0)
                    continue;

                if (HasAttributeValue(child, key, value))
                {
                    return child.TextContentClean;
                }
                var s = GetStringFromText(child, key, value);
                if (!string.IsNullOrEmpty(s))
                    return s;
            }

            return "";
        }

        public static string GetStringFromKey(HtmlNode node, string attributeKey)
        {
            if (node == null)
                return string.Empty;

            if (node.Attributes.ContainsKey(attributeKey))
                return node.Attributes[attributeKey];
            else
            {
                foreach (var child in node.ChildNodes)
                {
                    if (child == null)
                        continue;

                    if (child.Attributes.Count <= 0)
                        continue;


                    if (child.Attributes.ContainsKey(attributeKey))
                        return child.Attributes[attributeKey];

                    var s = GetStringFromKey(child, attributeKey);
                    if (!string.IsNullOrEmpty(s))
                        return s;
                }
            }
            return "";
        }

        public static bool HasAttributeValue(HtmlNode node, string key, string value)
        {
            foreach (var item in node.Attributes)
            {
                if (item.Value == key)
                {
                    return node.TextContentClean.Contains(value);
                }
            }
            return false;
        }
    }
}
