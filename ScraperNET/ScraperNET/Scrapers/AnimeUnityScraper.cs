﻿using System;
using System.Collections.Generic;
using System.Text;
using IronWebScraper;
using System.Linq;
using ScraperNET.Common;
using ScraperNET.Models;
using Newtonsoft.Json;
using System.Threading;
using System.IO;
using ScraperNET.Utils;

namespace ScraperNET.Scrapers
{
    public class AnimeUnityScraper : WebScraper, IScraper
    {
        public bool IsRequestAvaible
        {
            get
            {
                if (this._requestDone == 99 || this._requestDone == 100)
                {
                    ConsoleApp.RunNewInstance(this.metaData);
                    return false;

                }

                return true;
            }
        }

        public AnimeMetadata metaData { get { return ConsoleApp.metaData; } set { ConsoleApp.metaData = value; } }

        private AnimeModel _currentAnime;

        private int _requestDone = 0;

        public AnimeUnityScraper()
        {
            this._requestDone = 0;
            this.OpenConnectionLimitPerHost = 30;
            Console.WriteLine("Request : " + this.SuccessfulfulRequestCount);
            this.MaxHttpConnectionLimit = 30;
            License.LicenseKey = "";//"IRONSCRAPER-MYLICENSE-KEY-1EF01"; // Write License Key
            this.LoggingLevel = WebScraper.LogLevel.All; // All Events Are Logged
        }

        public void Resume(AnimeMetadata animeMetadata,bool rescrape = false)
        {
            this.metaData = animeMetadata;

            if (rescrape && this.metaData.version == 1)
            {
                for (int i = 0; i < this.metaData.animes.Count; i++)
                {
                    this.metaData.animes[i].scraped = false;
                }
            }

            var animesNotScrapedYet = this.metaData.animes.FindAll(anime => !anime.scraped);

            if (animesNotScrapedYet.Count == 0)
            {
                this.metaData.archiveCompletelyScraped = true;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("JSON COMPLETED, ANIME UNITY SCRAPED SUCCESFULLY ");
                Console.ReadLine();

                return;
            }

            var animeModel = animesNotScrapedYet[0];//.Find(a => a.title == "One Piece");

            if (_requestDone < 98)
            {
                Console.WriteLine("Scraping episode for: " + animeModel.title);

                RequestEPPage(animeModel, () =>
                {
                    int index = this.metaData.animes.FindIndex(a => a.title == animeModel.title);

                    this.metaData.animes[index] = animeModel;

                    Resume(this.metaData,false);
                });
            }
            else
            {
                Console.WriteLine("Requests Finished: ");


                ConsoleApp.RunNewInstance(this.metaData);
            }

        }

        public void RequestEPPage(AnimeModel animeModel, Action onComplete)
        {
            this.Request(animeModel.firstEpisode, (Response response) =>
            {
                ReadEpisodePage(response, animeModel, () =>
                {
                    animeModel.scraped = true;

                    Console.WriteLine("Anime scraped completely : " + animeModel.title);

                    onComplete?.Invoke();
                });
            });
        }

        public override void Init()
        {
            if (File.Exists(ConsoleApp.JsonPath))
            {
                var metaData = JsonConvert.DeserializeObject<AnimeMetadata>(File.ReadAllText(ConsoleApp.JsonPath));

                this.metaData = metaData;
            }

            if (this.metaData.version == 0)
                this.Request("https://www.animeunity.it/anime.php?c=archive&page=*", Parse);
            else
            {
                Resume(metaData,metaData.rescrape);
            }
        }

        public override void Request(string url, Action<Response> parse)
        {
            lock (this)
            {
                this._requestDone++;

                if (this.IsRequestAvaible)
                {
                    base.Request(url, parse);

                    Console.WriteLine("Request Done: " + this._requestDone);
                }
            }
        }

        public override void Parse(Response response)
        {
            // set working directory for the project
            this.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory + "/Output";//@"C:/Progetti/AnimeUnityScraper/Output/";

            this.ReadMainPage(response);
        }

        public void ReadMainPage(Response response)
        {
            var archiveContainer = response.Css(AnimeUnityRules.ANIME_CSS_DIV_ARCHIVECONTAINER_SELECTOR);
            if (archiveContainer == null || archiveContainer.Length == 0)
            {
                Console.WriteLine("Error: archive container null");
                return;
            }

            var animes = NodeHelper.GetNodesFiltered(AnimeUnityRules.ANIME_CSS_CLASS, archiveContainer[0].ChildNodes.ToList()); /*response.Css(AnimeUnityRules.ANIME_CSS_CLASS).ToList();*/

            if (animes == null || animes.Count == 0)
                return;

            if (!this.metaData.archiveCompletelyScraped)
                this.ScrapeArchive(animes);
        }

        public void ReadEpisodePage(Response response, AnimeModel animeModel, Action onComplete)
        {
            var episodeList = response.Css(AnimeUnityRules.ANIME_CSS_DIV_EPISODECONTAINER_SELECTOR);
            var epTabs = response.Css("ul.nav-tabs#eps");

            episodeList = NodeHelper.GetNodesFiltered("ep-box col-lg-1 col-sm-1", episodeList.ToList()).ToArray();

            if (episodeList.Length == animeModel.episodes.Count)
            {
                onComplete?.Invoke();
                return;
            }

            if (episodeList.Length == 0)
            {
                animeModel.episodes.Add(new EpisodeModel()
                {
                    episodeLink = animeModel.firstEpisode
                });
            }

            foreach (var ep in episodeList)
            {
                EpisodeModel episode = new EpisodeModel();

                episode.episodeLink = NodeHelper.GetStringFromKey(ep, AnimeUnityRules.LINK);

                if (!animeModel.episodes.Exists(e => e.episodeLink == episode.episodeLink))
                    animeModel.episodes.Add(episode);
            }

            int epScrapedCount = animeModel.episodes.FindAll(e => e.epScraped).Count;

            foreach (var ep in animeModel.episodes)
            {
                try
                {
                    if (ep.epScraped)
                        continue;

                    if (!this.IsRequestAvaible)
                    {
                        onComplete?.Invoke();
                        return;
                    }

                    try
                    {
                        this.Request(ep.episodeLink, (Response response1) =>
                        {
                            ep.epScraped = true;

                            epScrapedCount++;

                            var videoPlayer = response1.Css("#video-player");

                            var epTitle = response1.Css(".cus_title");

                            if (videoPlayer == null || videoPlayer.Length == 0)
                            {
                                Console.WriteLine("Anime without videoUrl: " + this._currentAnime.title);
                                return;
                            }

                            ep.urlVideo = NodeHelper.GetStringFromKey(videoPlayer[0], AnimeUnityRules.SRC);
                            ep.title = epTitle[0].TextContentClean;

                            Console.WriteLine("Episode scraped: " + ep.title);

                            if (epScrapedCount == animeModel.episodes.Count)
                            {
                                Console.WriteLine("anime : " + animeModel.title);
                                onComplete?.Invoke();
                            }
                        });

                        Thread.Sleep(100);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + ep.title + " " + ep.title);
                }
            }

            while (epScrapedCount < animeModel.episodes.Count)
            {

            }

        }

        private void ScrapeArchive(List<HtmlNode> container)
        {
            for (int i = 0; i < container.Count; i++)
            {
                if (!this.IsRequestAvaible)
                    return;

                AnimeModel animeModel = new AnimeModel();

                this._currentAnime = animeModel;

                var animeScraped = container[i];

                var imgNode = NodeHelper.GetNodeForValue(animeScraped, AnimeUnityRules.ANIME_CSS_IMG_CLASS);
                var contentNode = NodeHelper.GetNodeForValue(animeScraped, AnimeUnityRules.ANIME_CSS_CONTENT_CLASS);

                if (imgNode != null) // Get anime web page and img 
                {
                    var webPage = NodeHelper.GetStringFromKey(imgNode, AnimeUnityRules.LINK);
                    var imgLink = NodeHelper.GetStringFromKey(imgNode, AnimeUnityRules.SRC);

                    if (!string.IsNullOrEmpty(webPage))
                        animeModel.firstEpisode = webPage;

                    if (!string.IsNullOrEmpty(imgLink))
                        animeModel.imgLink = imgLink;
                }

                if (contentNode != null)
                {
                    var titleNode = NodeHelper.GetNodeForValue(contentNode, AnimeUnityRules.ANIME_CSS_TITLE);
                    var genresNode = NodeHelper.GetNodeForValue(contentNode, "card-footer archive-card-footer");
                    var descriptionNode = NodeHelper.GetNodeForValue(contentNode, AnimeUnityRules.ANIME_CSS_DESCRIPTION);

                    var episodesNode = NodeHelper.GetStringFromText(contentNode, "card-text", "Numero Episodi:");
                    var releaseDate = NodeHelper.GetStringFromText(contentNode, "card-text", "Anno di uscita:");
                    var genres = NodeHelper.GetNodesFiltered("badge btn-archive-genres", new List<HtmlNode>() { genresNode });

                    if (titleNode != null)
                        animeModel.title = titleNode.TextContentClean;

                    if (!string.IsNullOrEmpty(episodesNode))
                    {
                        try
                        {
                            var numberEpisode = Convert.ToInt32(episodesNode.Split(':')[1]);
                        }
                        catch (Exception e)
                        {

                        }

                    }

                    if (!string.IsNullOrEmpty(releaseDate))
                    {
                        var releaseDateNumber = Convert.ToInt32(releaseDate.Split(':')[1]);
                        animeModel.releaseDate = releaseDateNumber;
                    }

                    if (genres != null && genres.Count > 0)
                    {
                        foreach (var gNode in genres)
                        {
                            string category = gNode.TextContentClean;

                            if (string.IsNullOrEmpty(category)) continue;
                            if (animeModel.generes.Exists(c => c == category)) continue;
                            animeModel.generes.Add(category);
                        }
                    }
                }

                if (!this.metaData.animes.Exists(anime => anime.title == animeModel.title))
                    this.metaData.animes.Add(animeModel);
            }

            Console.WriteLine("Done!!");

            ConsoleApp.RunNewInstance(this.metaData);
        }
    }
}
