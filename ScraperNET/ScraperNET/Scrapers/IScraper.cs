﻿using IronWebScraper;
using ScraperNET.Models;
using System;

namespace ScraperNET.Scrapers
{
    interface IScraper
    {
        AnimeMetadata metaData { get; set; }

        void ReadEpisodePage(Response response, AnimeModel animeModel, Action onComplete);

        void RequestEPPage(AnimeModel animeModel, Action onComplete);

        void Resume(AnimeMetadata animeMetadata,bool rescrape = false);

        void ReadMainPage(Response response);
    }
}
