﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronWebScraper;
using Newtonsoft.Json;
using ScraperNET.Common;
using ScraperNET.Models;
using ScraperNET.Utils;

namespace ScraperNET.Scrapers
{
    //https://www.animesaturn.com/animelist
    public class AnimeSaturnScraper : WebScraper, IScraper
    {
        public AnimeMetadata metaData { get { return ConsoleApp.metaData; } set { ConsoleApp.metaData = value; } }

        public bool IsRequestAvaible
        {
            get
            {
                if (this._requestDone == 99 || this._requestDone == 100)
                {
                    ConsoleApp.RunNewInstance(this.metaData);
                    return false;

                }

                return true;
            }
        }

        private int _requestDone = 0;

        public override void Init()
        {
            if (File.Exists(ConsoleApp.JsonPath))
            {
                var metaData = JsonConvert.DeserializeObject<AnimeMetadata>(File.ReadAllText(ConsoleApp.JsonPath));

                this.metaData = metaData;
            }

            if (this.metaData.version == 0)
                this.Request("https://www.animesaturn.com/animelist?load_all=1", Parse);
            else
            {
                Resume(metaData);
            }
        }

        public override void Request(string url, Action<Response> parse)
        {
            lock (this)
            {
                this._requestDone++;

                if (this.IsRequestAvaible)
                {
                    base.Request(url, parse);

                    Console.WriteLine("Request Done: " + this._requestDone);
                }
            }
        }

        public override void Parse(Response response)
        {
            this.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory + "/Output";//@"C:/Progetti/AnimeUnityScraper/Output/";

            this.ReadMainPage(response);
        }

        public void ReadEpisodePage(Response response, AnimeModel animeModel, Action onComplete)
        {
            var episodeList = response.Css("#resultsxd");
            var epTabs = response.Css("ul.nav-tabs#eps");
        }

        public void ReadMainPage(Response response)
        {
            var archiveContainer = response.Css("li");//AnimeUnityRules.ANIME_CSS_DIV_ARCHIVECONTAINER_SELECTOR);
            if (archiveContainer == null || archiveContainer.Length == 0)
            {
                Console.WriteLine("Error: archive container null");
                return;
            }

            foreach (var node in archiveContainer[0].ChildNodes)
            {
                if (node == null || node.Attributes.Count == 0) continue;

                if (!ConsoleApp.metaData.animes.Exists(a => a.title == node.TextContentClean))
                {
                    var anime = new AnimeModel() { title = node.TextContentClean, firstEpisode = NodeHelper.GetStringFromKey(node, "href") };

                    Request(anime.firstEpisode,(Response r) => 
                    {
                        ReadEpisodePage(r, anime, null);
                    });
                    
                }
            }
        }

        public void RequestEPPage(AnimeModel animeModel, Action onComplete)
        {
            
        }

        public void Resume(AnimeMetadata animeMetadata,bool rescrape = false)
        {
            
        }
    }
}
